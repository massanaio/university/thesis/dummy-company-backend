package com.dummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DummyCompanyBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyCompanyBackendApplication.class, args);
	}
}
