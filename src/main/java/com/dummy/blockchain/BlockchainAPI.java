package com.dummy.blockchain;

import com.dummy.config.BlockchainProperties;
import com.dummy.blockchain.blockchainAccessObjects.VerificationDocumentsBAO;
import com.dummy.server.dto.VerificationDocumentsDTO;
import com.dummy.blockchain.blockchainAccessObjects.VerificationIdentifierBAO;
import com.dummy.server.dto.VerificationIdentifierDTO;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class BlockchainAPI {

    private final BlockchainProperties blockchainProperties;
    private final DozerBeanMapper mapper;

    public BlockchainAPI(BlockchainProperties blockchainProperties, DozerBeanMapper mapper) {
        this.blockchainProperties = blockchainProperties;
        this.mapper = mapper;
    }

    public String storeUserRecord(VerificationDocumentsDTO verificationDocumentsDTO) {
        final String uri = "http://localhost:3000/addUser";

        VerificationDocumentsBAO verificationDocumentsBAO = mapper.map(verificationDocumentsDTO, VerificationDocumentsBAO.class);
        verificationDocumentsBAO.setSender(blockchainProperties.getAccount());

        RestTemplate restTemplate = new RestTemplate();
        String identifier = restTemplate.postForObject(uri, verificationDocumentsBAO, String.class);

        if(identifier.equalsIgnoreCase("Error")){
            throw new RuntimeException("Blockchain Error");
        }

        log.info("User stored in blockchain with identifier {}", identifier);

        return identifier;
    }

    public Boolean checkRecord(VerificationIdentifierDTO verificationIdentifierDTO) {
        final String uri = "http://localhost:3000/checkUserIsVerified";

        VerificationIdentifierBAO verificationIdentifierBAO = mapper.map(verificationIdentifierDTO, VerificationIdentifierBAO.class);

        RestTemplate restTemplate = new RestTemplate();
        Boolean isVerified = restTemplate.postForObject(uri, verificationIdentifierBAO, Boolean.class);

        return isVerified;
    }
}
