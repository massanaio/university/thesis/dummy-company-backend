package com.dummy.blockchain.blockchainAccessObjects;

import lombok.Data;

@Data
public class VerificationDocumentsBAO {
    private String name;
    private String documentNumber;
    private String address;
    private String sender;
}
