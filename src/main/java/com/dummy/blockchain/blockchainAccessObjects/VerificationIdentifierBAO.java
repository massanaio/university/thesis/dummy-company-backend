package com.dummy.blockchain.blockchainAccessObjects;

import lombok.Data;

@Data
public class VerificationIdentifierBAO {
    private String userIdentifier;
    private String documentNumber;
}
