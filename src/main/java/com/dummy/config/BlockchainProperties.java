package com.dummy.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class BlockchainProperties {

    private final String account;
    private final String passphrase;
    private final String privateKey;

    public BlockchainProperties(
            @Value("${blockchain.account}") String account,
            @Value("${blockchain.passphrase}") String passphrase,
            @Value("${blockchain.privateKey}") String privateKey
    ) {
        this.account = account;
        this.passphrase = passphrase;
        this.privateKey = privateKey;
    }


}
