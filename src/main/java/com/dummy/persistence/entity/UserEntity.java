package com.dummy.persistence.entity;

import com.dummy.util.enums.VerificationStatus;
import com.dummy.util.enums.VerificationType;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "documentNumber", unique = true)
    private String documentNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "verification_status")
    private VerificationStatus verificationStatus;

    @Column(name = "verification_type")
    private VerificationType verificationType;

    @Column(name = "blockchain_identifier", unique = true)
    private String blockchainIdentifier;

}
