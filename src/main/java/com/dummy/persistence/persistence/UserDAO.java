package com.dummy.persistence.persistence;

import com.dummy.persistence.entity.UserEntity;
import com.dummy.util.enums.VerificationStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDAO extends JpaRepository<UserEntity, Long> {
    List<UserEntity> findAll();
    UserEntity findByDocumentNumber(String documentNumber);
    List<UserEntity> findAllByVerificationStatus(VerificationStatus verificationStatus);
}
