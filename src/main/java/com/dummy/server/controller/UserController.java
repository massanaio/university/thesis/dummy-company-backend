package com.dummy.server.controller;

import com.dummy.server.dto.UserCreateDTO;
import com.dummy.server.dto.UserDTO;
import com.dummy.server.service.UserService;
import com.dummy.util.enums.VerificationStatus;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@Api(tags = "Users")
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    @GetMapping("/{documentNumber}")
    public UserDTO findByDocumentNumber(@PathVariable String documentNumber){
        return userService.findByDocumentNumber(documentNumber);
    }

    @PostMapping
    public UserDTO create(@RequestBody UserCreateDTO user){
        return userService.create(user);
    }



}
