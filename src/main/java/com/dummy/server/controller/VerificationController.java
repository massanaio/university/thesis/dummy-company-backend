package com.dummy.server.controller;

import com.dummy.server.dto.UserDTO;
import com.dummy.server.dto.VerificationDocumentsDTO;
import com.dummy.server.dto.VerificationIdentifierDTO;
import com.dummy.server.service.IdentityVerificationService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@Api(tags = "Verification")
@RequestMapping("/api/identityVerification")
public class VerificationController {

    @Autowired
    private IdentityVerificationService identityVerificationService;

    @PostMapping("/documents")
    public UserDTO verifyThroughDocuments(@RequestBody VerificationDocumentsDTO verificationDocumentsDTO){
        return identityVerificationService.verifyThroughDocuments(verificationDocumentsDTO);
    }

    @PostMapping("/identifier")
    public UserDTO verifyThroughIdentifier(@RequestBody VerificationIdentifierDTO verificationIdentifierDTO){
        return identityVerificationService.verifyThroughIdentifier(verificationIdentifierDTO);
    }

}

