package com.dummy.server.dto;

import lombok.Data;

@Data
public class UserCreateDTO {

    private String name;
    private String documentNumber;
    private String address;

}
