package com.dummy.server.dto;

import com.dummy.util.enums.VerificationStatus;
import com.dummy.util.enums.VerificationType;
import lombok.Data;

@Data
public class UserDTO {

    private String name;
    private String documentNumber;
    private String address;
    private VerificationStatus verificationStatus;
    private VerificationType verificationType;
    private String blockchainIdentifier;

}
