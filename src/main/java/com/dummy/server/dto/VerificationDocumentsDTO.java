package com.dummy.server.dto;

import lombok.Data;

@Data
public class VerificationDocumentsDTO {
    private String name;
    private String documentNumber;
    private String address;
}
