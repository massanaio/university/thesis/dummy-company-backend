package com.dummy.server.dto;

import lombok.Data;

@Data
public class VerificationIdentifierDTO {
    private String userIdentifier;
    private String documentNumber;
}
