package com.dummy.server.service;

import com.dummy.server.dto.UserDTO;
import com.dummy.server.dto.VerificationDocumentsDTO;
import com.dummy.server.dto.VerificationIdentifierDTO;

public interface IdentityVerificationService {

    UserDTO verifyThroughDocuments(VerificationDocumentsDTO verificationDocumentsDTO);
    UserDTO verifyThroughIdentifier(VerificationIdentifierDTO verificationIdentifierDTO);
}
