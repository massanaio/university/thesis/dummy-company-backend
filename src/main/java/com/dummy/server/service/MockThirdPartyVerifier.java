package com.dummy.server.service;

import com.dummy.server.dto.VerificationDocumentsDTO;

public interface MockThirdPartyVerifier {

    Boolean verifyUser(VerificationDocumentsDTO verificationDocumentsDTO);

}
