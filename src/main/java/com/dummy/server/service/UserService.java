package com.dummy.server.service;

import com.dummy.server.dto.UserCreateDTO;
import com.dummy.server.dto.UserDTO;

import java.util.List;

public interface UserService {
    UserDTO create(UserCreateDTO userCreateDTO);
    List<UserDTO> findAll();
    UserDTO findByDocumentNumber(String documentNumber);
}
