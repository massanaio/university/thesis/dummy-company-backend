package com.dummy.server.service.impl;

import com.dummy.blockchain.BlockchainAPI;
import com.dummy.persistence.entity.UserEntity;
import com.dummy.persistence.persistence.UserDAO;
import com.dummy.server.dto.UserDTO;
import com.dummy.server.dto.VerificationDocumentsDTO;
import com.dummy.server.dto.VerificationIdentifierDTO;
import com.dummy.server.service.IdentityVerificationService;
import com.dummy.util.enums.VerificationStatus;
import com.dummy.util.enums.VerificationType;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IdentityVerificationServiceImpl implements IdentityVerificationService {

    private final MockThirdPartyVerifierImpl mockThirdPartyVerifier;
    private final BlockchainAPI blockchainAPI;
    private final UserDAO userDAO;
    private final DozerBeanMapper mapper;

    public IdentityVerificationServiceImpl(MockThirdPartyVerifierImpl mockThirdPartyVerifier, BlockchainAPI blockchainAPI, UserDAO userDAO, DozerBeanMapper mapper) {
        this.mockThirdPartyVerifier = mockThirdPartyVerifier;
        this.blockchainAPI = blockchainAPI;
        this.userDAO = userDAO;
        this.mapper = mapper;
    }

    @Override
    public UserDTO verifyThroughDocuments(VerificationDocumentsDTO verificationDocumentsDTO) {
        UserEntity user = sendToThirdPartyVerifier(verificationDocumentsDTO);
        return mapper.map(user, UserDTO.class);
    }

    @Override
    public UserDTO verifyThroughIdentifier(VerificationIdentifierDTO verificationIdentifierDTO) {
        UserEntity user;
        Boolean isVerified = blockchainAPI.checkRecord(verificationIdentifierDTO);
        if(isVerified){
            user = updateUserOnSuccessfulVerification(verificationIdentifierDTO.getDocumentNumber(), verificationIdentifierDTO.getUserIdentifier(), VerificationType.BLOCKCHAIN_CHECK);
            return mapper.map(user, UserDTO.class);
        } else {
            throw new RuntimeException("This user does not exist on the blockchain");
        }
    }

    @Scheduled(fixedDelay = 20000, initialDelay = 20000)
    public void simulateThirdPartyVerifications(){
        List<UserEntity> usersInProcessOfVerification = userDAO.findAllByVerificationStatus(VerificationStatus.IN_PROCESS);

        if(!usersInProcessOfVerification.isEmpty()){
            log.info("Running simulateThirdPartyVerifications");
            UserEntity userToVerify = usersInProcessOfVerification.get(0);

            VerificationDocumentsDTO verificationDocumentsDTO = new VerificationDocumentsDTO();

            verificationDocumentsDTO.setName(userToVerify.getName());
            verificationDocumentsDTO.setDocumentNumber(userToVerify.getDocumentNumber());
            verificationDocumentsDTO.setAddress(userToVerify.getAddress());

            storeUserRecordInBlockchain(mapper.map(userToVerify, VerificationDocumentsDTO.class));
        }
    }

    public UserEntity sendToThirdPartyVerifier(VerificationDocumentsDTO verificationDocumentsDTO) {
        UserEntity user = userDAO.findByDocumentNumber(verificationDocumentsDTO.getDocumentNumber());
        user.setVerificationStatus(VerificationStatus.IN_PROCESS);

        return userDAO.save(user);
    }

    private UserEntity storeUserRecordInBlockchain(VerificationDocumentsDTO verificationDocumentsDTO){
        String identifier = blockchainAPI.storeUserRecord(verificationDocumentsDTO);
        if(identifier != "Error"){
            return updateUserOnSuccessfulVerification(verificationDocumentsDTO.getDocumentNumber(), identifier, VerificationType.THIRD_PARTY_VERIFIER);
        } else {
            throw new RuntimeException("Error storing record in the Blockchain");
        }
    }

    private UserEntity updateUserOnSuccessfulVerification(String documentNumber, String blockchainIdentifier, VerificationType verificationType){
        UserEntity user = userDAO.findByDocumentNumber(documentNumber);
        user.setVerificationStatus(VerificationStatus.VERIFIED);
        user.setVerificationType(verificationType);
        user.setBlockchainIdentifier(blockchainIdentifier);
        return userDAO.save(user);
    }
}
