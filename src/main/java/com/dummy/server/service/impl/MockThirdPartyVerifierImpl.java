package com.dummy.server.service.impl;

import com.dummy.server.dto.VerificationDocumentsDTO;
import com.dummy.server.service.MockThirdPartyVerifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class MockThirdPartyVerifierImpl implements MockThirdPartyVerifier {

    @Override
    public Boolean verifyUser(VerificationDocumentsDTO verificationDocumentsDTO) {
        log.info("Verifying...");
        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Finished verifying...");
        return true;
    }

}
