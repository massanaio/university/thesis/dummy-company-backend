package com.dummy.server.service.impl;

import com.dummy.persistence.entity.UserEntity;
import com.dummy.persistence.persistence.UserDAO;
import com.dummy.server.dto.UserCreateDTO;
import com.dummy.server.dto.UserDTO;
import com.dummy.server.service.UserService;
import com.dummy.util.MappingHelper;
import com.dummy.util.enums.VerificationStatus;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;
    private final DozerBeanMapper mapper;

    public UserServiceImpl(UserDAO userDAO, DozerBeanMapper mapper) {
        this.userDAO = userDAO;
        this.mapper = mapper;
    }

    @Override
    public List<UserDTO> findAll() {
        return userDAO.findAll().stream()
                .map(userEntity -> mapper.map(userEntity, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO create(UserCreateDTO userCreateDTO) {
        UserEntity user = mapper.map(userCreateDTO, UserEntity.class);
        user.setVerificationStatus(VerificationStatus.PENDING);
        user.setVerificationType(null);

        return mapper.map(userDAO.save(user), UserDTO.class);
    }

    @Override
    public UserDTO findByDocumentNumber(String documentNumber) {
        log.info("Retrieving user with document number {}", documentNumber);
        UserEntity userEntity = userDAO.findByDocumentNumber(documentNumber);
        return mapper.map(userEntity, UserDTO.class);
    }
}
