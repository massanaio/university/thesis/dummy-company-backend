package com.dummy.util;

import org.dozer.DozerBeanMapper;

import java.util.List;
import java.util.stream.Collectors;

public class MappingHelper {

    public static <T> List<T> mapList(DozerBeanMapper beanMapper, List collection, Class<T> clazz){
        return (List<T>) collection.stream().map((s) -> beanMapper.map(s, clazz)).collect(Collectors.toList());
    }
}