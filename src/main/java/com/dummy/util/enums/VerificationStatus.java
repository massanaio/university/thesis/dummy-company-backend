package com.dummy.util.enums;

public enum VerificationStatus {
    PENDING,
    IN_PROCESS,
    VERIFIED
}
