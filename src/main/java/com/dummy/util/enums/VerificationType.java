package com.dummy.util.enums;

public enum VerificationType {
    BLOCKCHAIN_CHECK,
    THIRD_PARTY_VERIFIER
}
